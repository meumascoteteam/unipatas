<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">

    <div class="table-responsive mx-auto">
        <table class="table table-hover table-striped" style="max-width:100%">
            <thead class="">
                <tr>
                    <th scope="col">Entrada / Saída</th>
                    <th scope="col">Preço</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Data</th>
                </tr>
            </thead>
            <tbody>
            <?php  foreach ($itens as $item): ?>
                <tr>
                 <td><i class="fa
                      <?= ($item->entrada_saida == "entrada") ? 
                          " fa-arrow-right text-success ":
                          " fa-arrow-left text-danger ";
                      ?>
                      icone-padrao">
                    </i></td>
                  <td>R$<?=$item->preco?></td>
                  <td><?=$item->descricao?></td>
                  <td><?=$item->data?></td>
                 </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


</div>
</div>
</div>


<?= asset_js("ws")?>
