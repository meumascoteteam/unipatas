<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row ml-1">
    <!-- DIREITA -->
    <div class="col-md-5 jumbotron ml-auto pt-5" id="div_direita">
        <div class="row">
            <div class="col-md-12">
                <h1 class="bd-title text-center" id="titulo"></h1>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-10 mx-auto">
                <p class="text-justify lead" id="descricao"></p>
            </div>
        </div>
        <hr />
        <div class="row">
            <!-- LISTA 1 -->
            <div class="col-md-6 mx-auto mb-3">
                <ul class="list-group  list-group-lg ">
                    <li class="list-group-item list-group-item-success">
                        Informações:
                    </li>
                    <li class="list-group-item list-group-item-default">
                        Castrado: <span id='castracao'></span> 
                    </li>
                    <li class="list-group-item list-group-item-default">
                        Porte: <span id='porte'></span>
                    </li>
                    <li class="list-group-item list-group-item-default">
                        Sexo: <span id='sexo'></span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">
                        Problemas médicos
                    </li>
                    <li class="list-group-item list-group-item-default">
                        <p>Este animal não possui problemas médicos.</p>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-3">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">
                        Medicamentos
                    </li>
                    <li class="list-group-item list-group-item-default">
                        <p>Nenhum medicamento encontrado.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-5 mr-auto">
        <div class="row">
            <div class="col-md-12">
                <img alt="Imagem do animal" src="" class="img-rounded" id="imagem-animal" />
            </div>
        </div>
    </div>
</div>
</div>
</div>


<?= asset_js("ws")?>
