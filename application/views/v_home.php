<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-fluid mt-5">
    <div class="row  justify-content-lg-center">
        <div class="col-md-4 col-lg-3">
            <a href="<?= base_url("home/animais") ?>">
                <button type="button" class="btn btn-success btn-lg btn-block p-4 mb-5 btn-home">Cães</button>
            </a>
        </div>
        <div class="col-md-4 col-lg-3">
            <a href="<?= base_url("home/como_ajudar") ?>">
                <button type="button" class="btn btn-dark btn-lg btn-block p-4 mb-5 btn-home">
                    <span>Como ajudar<span></button>
            </a>
        </div>
        <div class="col-md-4 col-lg-3">
            <a href="<?= base_url("home/portal") ?>">
                <button type="button" class="btn btn-success btn-lg btn-block p-4 mb-5 btn-home">
                    <span>Portal da transparência<span></button>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="text-center mb-4 col-md-12">
        </div>
    </div>
    <div class="row">
        <div class="alert alert-dark mx-auto mt-4 mb-4" role="alert">
            <h5 clas="text-center">Pedimos que não alimentem os cães em torno do RU. Obrigado</h5>
        </div>
    </div>
</div>

</div>
</div>

