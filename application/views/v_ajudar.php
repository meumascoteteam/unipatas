<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">

    <div class="col-sm-7 mx-auto">
        <div class="row">
            <div class="text-center mb-5 mt-2">
                <h1 class="text-center mb-3 bd-title"> Faça qualquer doação </h1> 
                <p class="lead">Você pode doar qualquer produto ou quantia. Porém, nós temos preferência por doação de produtos para serem utilizados pelos animais:<br /> Ração Apolo Original, vermífugo e Pour on antipulgas e carrapatos.<br /> Os produtos podem ser entregues na sala
                    205 do prédio da DTIC.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="text-center mb-1">
                <h1 class="text-center mb-3 bd-title"> Adote um animal </h1>
                <p class="lead"> Para adotar um dos nossos animais, basta entrar na página dos animais, escolher um dos cães, e entrar em contato conosco! </p>
            </div>
        </div>
        <div class="row mb-5">
            <div class="mx-auto">
                <a href="<?= base_url("home/animais") ?>">
                    <button type="button" class="btn btn-success btn-block btn-lg"> Ver animais </button>
                </a>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
</div>





