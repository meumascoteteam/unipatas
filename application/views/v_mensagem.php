<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Maurício El Uri">
        <title>Mensagem do site UNIPATAS</title>
        <?= asset_css("bootstrap.min") ?>
        <?= asset_css("styleRespostaEmail") ?>
        <?= asset_css("RespostaEmailBootstrapTheme") ?>
    </head>

    <body style='background-image: url(<?= asset_img('fundoRespostaEmail.jpg') ?>)'>
        <div id="wrapper">
            <div class="container">
                <div>
                    <a href="<?= base_url() ?>">
                        <h1>UNIPATAS</h1>
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= $tit ?></h1>
                        <h2 class="subtitle"><?= $texto ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="col-lg-6 col-lg-offset-3">
                <p class="copyright">UNIPATAS</p>
            </div>
        </footer>
    </body>
</html>
