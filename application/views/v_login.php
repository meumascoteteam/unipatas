<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>

<html>
    <head>
        <title>Login MeuMascote</title>
        <meta name = viewport content = "width=device-width, initial-scale=1">
        <meta charset = "UTF-8">
        <?= asset_css('bootstrap.min') ?>
        <?= asset_js('jquery-2.1.4.min') ?>
        <?= asset_css('login') ?>
        <?= asset_js('login') ?>

        <style>
body{
    background-color: #157BB5;
    background: url("<?= asset_img('bg_login.jpeg') ?>") no-repeat center center;
    background-size: cover;
} 
        </style>
        <!--[if lt IE 9]>
<script>
alert('Você está usando um navegador antigo!\n\nAlguns itens podem não aparecer corretamente.\nConsidere atualizar para uma versão mais recente.');
</script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="login-container">
                <div id="resultado"></div>
                <div class="avatar">
                    <i class="fa fa-user-circle fa-5x" aria-hidden="true"></i>
                </div>
                <div class="form-box">
                    <form action="asldkfj" method="POST">
                        <?= $input ?>
                        <input name='senha' type='password' placeholder='Senha' required />
                        <input type="hidden" name="token" value="<?= $token ?>" required />
                        <input type="hidden" name="link" value="<?= $link ?>" required />
                        <button class="btn btn-info btn-block login" type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<?= asset_js('bootstrap.min') ?>
<?= asset_css('font-awesome.min') ?>
