<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<footer>
    <div class="footer container-fluid pb-4" id="footer">
        <div class="row row-footer mx-auto">
            <div class="col-lg-3  col-md-3 col-sm-6">
                <h3>Projeto</h3>
                <a href="https://bitbucket.org/meumascoteteam/unipatas" target="_blank" class="">
                   Colabore!
                </a>
                <br /> 
                <a href="https://extensao.unipampa.edu.br/meumascote/home/login/site" target="_blank">
                   Área admin
                </a>
            </div>
            <div class="col-lg-5  col-md-4 col-sm-6">
                <h3>Parceiros</h3>
                <img class="logo-unipampa" src="<?=asset_img("logo-unipampa.png") ?> "/>
            </div>
            <div class="col-lg-4  col-md-5 col-sm-12">
                <h3>Entre em contato</h3>
                <form action="<?= base_url('email/form_simp') ?> " method="POST">
                    <div class="input-group">
                            <input name="nome" type="text" class="form-control" placeholder="Nome" required />
                            <input name="email" class="form-control" type="email" placeholder="Email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required />
                    </div>
                    <textarea name="mensagem" class="form-control" placeholder="Mensagem" rows=1 required></textarea>
                    <button class="btn btn-secondary btn-block" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>
    <!--/.footer-->
    <div class="footer-bottom">
        <div class="container-fluid pt-3">
            <div class="row row-footer mx-auto text-muted">
                <p class="pull-left pl-">Maurício El Uri - 2018</p>
            </div>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>

</body>

</html>
<?= asset_js('bootstrap.min') ?>
<?= asset_js('js') ?>
<?= asset_css('fonts') ?>
<?= asset_css('font-awesome.min') ?>
