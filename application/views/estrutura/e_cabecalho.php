<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
    <head>
        <title>Unipatas</title>
        <?= asset_css('bootstrap4-beta.min') ?>
        <?= asset_css('main') ?>
        <?= asset_js('jquery-2.1.4.min') ?>
    </head>
    <body>
        <div class="container-fluid p-3">
            <div class="header clearfix col-md-12">
                <div class="row mb-5">
                    <a href="<?= base_url() ?>" class="mx-auto d-block">
                        <img src="<?= asset_img("unipatas.png") ?>" alt="Logo Unipatas" id="logo"/>
                    </a>
                </div>
