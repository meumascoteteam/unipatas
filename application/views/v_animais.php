<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-sm-2">

        <button type="button" class="btn btn-dark btn-block btn-lg disabled filter-button" data-filter="all">Ver todos</button>
        <br />
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4>Seja padrinho de: </h4>
            </div>
            <div class="list-group">
                <button type="button" class="btn btn-lg btn-block btn-success filter-button mb-1" data-filter="antipulgas">Antipulgas</button>
                <button type="button" class="btn btn-lg btn-block btn-success filter-button mb-1" data-filter="castracao">Castração</h4></button>
                <!-- <button type="button" class="btn btn-lg btn-block btn-success filter-button mb-1" data-filter="racao">Ração</h4></button>-->
                <button type="button" class="btn btn-lg btn-block btn-success filter-button" data-filter="vacinas">Vacinas</h4></button>
            </div>
        </div>
    </div>
    <div class="col-sm-10">

        <section class="row" id="lista-animais">
            <div style="margin: 30px">
                <i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
                <br />
                <br />
                <span class="">Carregando imagens...</span>
            </div>
        </section>

    </div>
</div>
</div>
</div>



<?= asset_js("ws")?>
