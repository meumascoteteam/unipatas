<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('session.cache_limiter', 'public');
session_cache_limiter(FALSE);

function sec_login($tipo_login) {
		switch($tipo_login){
		case 'admin':
				return isset($_SESSION['sec_usuario']);
		default:
				return FALSE;
		}
		return FALSE;
}

function sec_cria($user) {
		$_SESSION['sec_nome'] = (string) $user->nome;
		$_SESSION['sec_email'] = (string) $user->email;
}

function sec_admin_cria($admin) {
		$_SESSION['sec_usuario'] = (string) $admin;
}

function sec_destroy() {
		return session_destroy();
}

function sec_token($token) {
		$_SESSION['sec_token'] = $token;
}

function sec_testa_token($token) {
		return ($token == $_SESSION['sec_token']);
}


function testa_login($tipo_login) {
		if (sec_login($tipo_login) == FALSE) {
				switch($tipo_login){
				case 'admin':
						redirect("home/login/admin/");
				default:
						return FALSE;
				}
				return FALSE;
		}
}

//function sec_cria2($user) {
//    $_SESSION = array(
//        'sec_id' => (int) $user[0],
//        'sec_email' => (string) $user[1],
//        'sec_texto' => (string) $user[2]
//    );
//    return TRUE;
//}
//
//function sec_login2() {
//    if (isset($_SESSION["sec_email"])) {
//        return TRUE;
//    }
//    return FALSE;
//}
