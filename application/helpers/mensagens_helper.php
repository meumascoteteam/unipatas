<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function login_dados_incorretos() {
    return "Erro ao efetuar o login. Dados incorretos!";
}

function login_dados_corretos() {
    return "<p class='blue'>Login efetuado com sucesso!</p>";
}
