<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->helper(array('form', 'utility'));
        $this->load->library('form_validation');
    }

    public function teste(){
        $variaveis = array(
            'tit' => 'Sua mensagem foi enviada com sucesso!',
            'texto' => 'Obrigado por entrar em contato conosco!'
        );
        $this->load->view('v_mensagem', $variaveis);
    }
    public function form_simp(){
        $this->form_validation->set_rules('nome', 'Nome', 'required|max_length[255]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|max_length[255]');
        $this->form_validation->set_rules('mensagem', 'Mensagem', 'required|max_length[255]');

        $this->form_validation->set_message('max_length', 'O campo %s deve ter no máximo %s caractere(s).');
        if ($this->form_validation->run() == FALSE) {
            $this->index('f', validation_errors());
        } else {
            $msg_simples = array(
                'nome' => $this->input->post('nome'),
                'email' => $this->input->post('email'),
                'mensagem' => $this->input->post('mensagem')
            ); 

            $mensagem = $this->load->view('v_email_simples', $msg_simples, true);
            $this->index($this->envia_email($mensagem), 't');
        }
    }

    private function envia_email($msg){
        $config = Array(
            'protocol'	 => 'smtp',
            'smtp_host'	 => 'ssl://mail.customsys.com.br',
            'smtp_port'	 => '465',
            'smtp_user'	 => '',
            'smtp_pass'	 => '',
            'mailtype'	 => 'html',
            'charset'	 => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('mailer-mm@customsys.com.br', 'Webmaster MeuMascote');
        $this->email->to('mauriciom.eluri@gmail.com');
        $this->email->subject('Nova mensagem do site unipatas!');
        $this->email->message($msg);
        if ($this->email->send()) {
            echo "Mensagem Enviada com sucesso!";
            return TRUE;
        } else {
            show_error($this->email->print_debugger());
            return FALSE;
        }
    }

    public function index($resultado = NULL, $erros = NULL){
        if($resultado == 't'){
            $variaveis = array(
                'tit' => 'Sua mensagem foi enviada com sucesso!',
                'texto' => 'Obrigado por entrar em contato conosco!'
            );
        } else if ($resultado == 'f'){
            $variaveis = array(
                'tit' => 'Houve um erro ao enviar sua mensagem!',
                'texto' => $erros
            );
        }
        else {
            redirect(base_url()); 	
        }
        $this->load->view('v_mensagem', $variaveis);
    }
}
