<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->helper(array('form', 'utility', 'secao', 'mensagens'));
        $this->load->library(array('session', 'form_validation'));
    }

    /**
     * Página de amostra do sistema - nada funcional além do login, só front-end
     */
    public function index() {
        $this->view('v_home'); 
    }

    public function animais() {
        $this->view('v_animais'); 
    }
    public function ver_animal($hash = NULL) {
        if ($hash == NULL){
            redirect();
        } else{
            $this->view('v_animal'); 
        }
    }

    public function como_ajudar() {
        $this->view('v_ajudar'); 
    }
    public function portal() {
        $url = parse_url(base_url());
        $url = $url['scheme']."://".$url['host']."/";
        $json = file_get_contents($url . "meumascote/ws/get_itens_pt/unipatas"); 
        $variaveis['itens'] = json_decode($json);
        $this->view('v_portal', $variaveis); 
    }
    public function login($tipo_login = NULL){
    /*if($tipo_login == 'admin'){
    $variaveis['input'] = "<input name='usuario' type='text' placeholder='Usuario' required autofocus />";
    $variaveis['token'] = sha1(uniqid(mt_rand()));
    sec_token($variaveis['token']);
    $variaveis['link'] = 'admin/';
    $this->load->view('v_login', $variaveis);
    }*/
    }

    public function logout($tipo_login = NULL){
    /*		sec_destroy();
    if($tipo_login == 'admin'){
    redirect('home/login/' . $tipo_login);	
    }
     */
    }

    private function view($view, $variaveis = NULL) {
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view($view, $variaveis);
        $this->load->view('estrutura/e_rodape');
    }

}
