<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_token extends CI_Model {

    public function salva_token($email, $token, $tipo) {
        $dados = array(
            'email' => $email,
            'token' => $token,
            'tipo' => $tipo
        );
        return $this->db->insert('tokens', $dados);
    }

    public function deleta_token($token) {
        return $this->db->where('token', $token)->delete('tokens');
    }

    public function testa_token($token) {
        $this->db->from('tokens');
        return $this->db->where('token', $token)->get()->row('email');
    }

}
