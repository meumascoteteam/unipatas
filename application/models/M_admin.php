<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

    public function verifica_login($usuario, $senha) {
        if ($usuario && $senha) {
            $this->db->where('usuario', $usuario);
            $qr = $this->db->get('admin');
            foreach ($qr->result_array() as $row) {
                $hash = $row['senha'];
                if (password_verify($senha, $hash)) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }
}
