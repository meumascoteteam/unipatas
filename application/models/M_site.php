<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_site extends CI_Model {

    #testar
    public function verifica_login($usuario, $senha) {
        if ($usuario && $senha) {
            $this->db->where('usuario', $usuario);
            $qr = $this->db->get('admin');
            foreach ($qr->result_array() as $row) {
                $hash = $row['senha'];
                if (password_verify($senha, $hash)) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function adiciona($site) {
        return $this->db->insert('site', $site);
    }

    public function get_all() {
        $this->db->order_by("id", 'desc');
        return $this->db->get('site');
    }

    public function get_by_hash($hash) {
        $this->db->where('hash', $hash);
        return $this->db->get('site')->row();
    }
    public function get_by_usuario($usuario) {
        $this->db->where('usuario', $usuario);
        return $this->db->get('site')->row();
    }
    public function atualiza($site) {
        $this->db->where('id', $site['id']);
        return $this->db->update("site", $site);
    }

    public function deleta($id) {
        return $this->db->where('id', $id)->delete('site');
    }
}
