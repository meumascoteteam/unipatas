$(document).ready(function() {
    var urlCompleta = window.location;
    var host = urlCompleta.protocol + "//" + urlCompleta.host + "/";
    var baseUrl = host + urlCompleta.pathname.split('/')[1];
    // SE REMOVER O ANIMAIS, MUDAR PARA 2
    var urlLocal = urlCompleta.pathname.split('/')[3];
    var hash_animal = urlCompleta.pathname.split('/')[4];
    switch(urlLocal){
        case "animais":
            imagens_lista_animais();
            break;
        case "ver_animal":
            pagina_animal();
            break;
    }

    function imagens_lista_animais(){
        $.getJSON(host + "meumascote/ws/animais/9dda77635d3ba00e2b18bbf4fb0092f59c86566d", function(data) {
            var div_lista = $("#lista-animais");
            div_lista.empty();
            $.each(data, function(index) {
                var imagem = data[index]['imagem'];
                var animalUrl = baseUrl + "/home/ver_animal/" + data[index]['hash'];
                var codigo_imagem ="<div class='gallery_product col-lg-4 col-md-4 col-sm-6 col-xs-12 filter antipulgas castracao'><a href='" + animalUrl + "'><img src='" + imagem + "' class='img-responsive'/></a></div>";
                $(codigo_imagem).appendTo(div_lista); 
            });
        }); 
    }


    function pagina_animal(){
        verifica_pag_animal();
        $.getJSON(host + "meumascote/ws/animal/" + hash_animal, function(data) {
            var animalInfo = ['titulo', 'descricao', 'castracao', 'porte', 'sexo', 'situacao', 'imagem'];
            var divAnimal = [];
            $.each(animalInfo, function(index) {

                //BOTA O CONTEUDO DENTRO DE CADA CAMPO
                //FOI UTILIZADO O MESMO PADRAO EM TUDO,
                //ENTAO FOI AUTOMATIZADO
                $("#" + animalInfo[index]).html(data[animalInfo[index]]); 
                console.log(data[animalInfo[index]]);
                $('#imagem-animal').attr('src', data[animalInfo['6']]); 
            });
        });
    }

    function verifica_pag_animal(){
    }
});
