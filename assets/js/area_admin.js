/*
 * AJUSTAR LARGURA DO MENU
 */
function htmlbodyHeightUpdate() {
    var height3 = $( window ).height()
    var height1 = $('.nav').height()+50
    height2 = $('.main').height()
    if(height2 > height3){
        $('html').height(Math.max(height1,height3,height2)+10);
        $('body').height(Math.max(height1,height3,height2)+10);
    } else {
        $('html').height(Math.max(height1,height3,height2));
        $('body').height(Math.max(height1,height3,height2));
    }

}
$(document).ready(function () {

    htmlbodyHeightUpdate()
    $( window ).resize(function() {
        htmlbodyHeightUpdate()
    });
    $( window ).scroll(function() {
        height2 = $('.main').height()
        htmlbodyHeightUpdate()
    });


    /*
     * PESSOAL
     */
    //TROCA BOTÃO ALTERAR SENHA 
    $('#btn-alterar-senha').click( function(){
        senha_ativa = true; 
        $('#btn-alterar-senha').css('display','none');
        $('#input-alterar-senha').fadeIn('slow');
    });
    $('form').submit( function(){
        if(senha_ativa === true){
            $('#nova-senha').val($('#input-alterar-senha').val());
        }
    });

}); 
