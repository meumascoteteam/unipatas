$(document).ready(function() {
    
    
    /*function entrar(url) { 
        window.location.replace(url);
    }*/
    
    //base_url
    var l = window.location;
    var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
    base_url += "/";
                            
    $("form").submit(function(e){
        e.preventDefault(e);
            $usuario = $("input[name=usuario]");
            $senha = $("input[name=senha]");
            $token = $("input[name=token]");
            $link = $("input[name=link]");
            $("#resultado").removeClass('alert-success alert-danger');
            $("#resultado").addClass("alert alert-info animated fadeInUp").html("Autenticando...");
            
            //$("#resultado").html("Autenticando...");
            /**Função ajax nativa da jQuery, onde passamos como par�metro o endere�o do arquivo que queremos chamar, os dados que ir� receber, e criamos de forma encadeada a fun��o que ir� armazenar o que foi retornado pelo servidor, para poder se trabalhar com o mesmo */
            $.post(base_url + $link.val() + "autenticar/", {usuario: $usuario.val(), senha: $senha.val(), token: $token.val()},
                function (retorno) {
                    if (retorno === "Erro ao efetuar o login. Dados incorretos!"){
                        $("#resultado").removeClass('alert-info alert-success')
                        $("#resultado").addClass("alert alert-danger animated fadeInUp").html(retorno);;
                    } else {
                        $("#resultado").removeClass('alert-info alert-danger');
                        $("#resultado").addClass("alert alert-success animated fadeInUp").html(retorno);
                            window.location.replace(base_url + $link.val());
                    }
                    
                    
                } //function(retorno)
        
            ); //$.post()
    });
});
