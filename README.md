# UNIPATAS #

Repositório do site https://extensao.unipampa.edu.br/unipatas/

* Este repositório é livre. Portanto, sinta-se à vontade para entrar em contato e colaborar.

### Qual o objetivo deste software? ###

* Fornecer uma plataforma para publicar fotos dos animais sob cuidados da UNIPAMPA, e onde informar dados para receber auxílio para cuidados dos mesmos.

### SOFTWARE UTILIZANDO O SISTEMA MEUMASCOTE ###

* O sistema MeuMascote é a base deste site, onde se publicam os dados e informações dos animais.
* Para mais informações sobre o sistema MeuMascote, entre em contato.

### Dados para contato ###
* Maurício El Uri
* mauriciom.eluri@gmail.com

Obrigado!!!